const staticFramework = "framework-1"
const assets = [
  "index.html",
  "style.css",
  "icon-512.png",
  "icon-192.png",
  "imagem.svg"
];

self.addEventListener("install", installEvent => {
  installEvent.waitUntil(
    caches.open(staticFramework).then(cache => {
      cache.addAll(assets)
    })
  )
});
self.addEventListener("fetch", fetchEvent => {
    fetchEvent.respondWith(
      caches.match(fetchEvent.request).then(res => {
        return res || fetch(fetchEvent.request)
      })
    )
  });